#!/bin/sh

if [ $1 ]; then
	led=$1
else
	led=$(zenity  --list --height 580 --width 300  --text "LED" --radiolist  --column "Pick" --column "LED" TRUE logo:blue:bottom FALSE logo:blue:top FALSE logo:green:bottom FALSE logo:green:top FALSE logo:red:bottom FALSE logo:red:top FALSE pyra:blue:left mmc0 pyra:blue:mid FALSE pyra:blue:right mmc1 pyra:green:left FALSE pyra:green:mid FALSE pyra:green:right FALSE pyra:red:left FALSE pyra:red:mid FALSE pyra:red:right);
fi

minbright=0;
maxbright="$(cat /sys/class/leds/$led/max_brightness)"
curbright="$(cat /sys/class/leds/$led/brightness)"

if [ $2 ]; then
	newbright=$2;
else
	newbright=$(zenity --scale --text "pick a number" --min-value=$minbright --max-value=$maxbright --value=$curbright --step 1);
fi

echo $newbright > /sys/class/leds/$led/brightness
