#!/bin/sh

if [ $1 ]; then
	if [ $2 ]; then
	trigger=$2
	else
	trigger=$(zenity  --list  --text "Trigger for $led" --radiolist  --column "Pick" --column "Opinion" TRUE none FALSE kbd-scrolllock FALSE kbd-numlock FALSE kbd-capslock FALSE kbd-kanalock FALSE kbd-shiftlock FALSE kbd-altgrlock FALSE kbd-ctrllock FALSE kbd-altlock FALSE kbd-shiftllock FALSE kbd-shiftrlock FALSE kbd-ctrlllock FALSE kbd-ctrlrlock FALSE usb-gadget FALSE usb-host FALSE mmc1 FALSE timer FALSE oneshot FALSE disk-activity FALSE ide-disk FALSE heartbeat FALSE cpu0 FALSE cpu1 FALSE default-on FALSE panic FALSE mmc0 FALSE mmc2 FALSE mmc3 FALSE generic-adc-batt-charging-or-full FALSE generic-adc-batt-charging FALSE generic-adc-batt-full FALSE generic-adc-batt-charging-blink-full-solid FALSE bq27421-0-charging-or-full FALSE bq27421-0-charging FALSE bq27421-0-full FALSE bq27421-0-charging-blink-full-solid FALSE bq24297-online FALSE rfkill0);
	fi
led=$1
else
led=$(zenity  --list --height 570 --width 300 --text "LED" --radiolist  --column "Pick" --column "LED" TRUE logo:blue:bottom FALSE logo:blue:top FALSE logo:green:bottom FALSE logo:green:top FALSE logo:red:bottom FALSE logo:red:top FALSE pyra:blue:left mmc0 pyra:blue:mid FALSE pyra:blue:right mmc1 pyra:green:left FALSE pyra:green:mid FALSE pyra:green:right FALSE pyra:red:left FALSE pyra:red:mid FALSE pyra:red:right);
trigger=$(zenity  --list --height 500 --width 300 --text "Trigger for $led" --radiolist  --column "Pick" --column "Opinion" TRUE none FALSE kbd-scrolllock FALSE kbd-numlock FALSE kbd-capslock FALSE kbd-kanalock FALSE kbd-shiftlock FALSE kbd-altgrlock FALSE kbd-ctrllock FALSE kbd-altlock FALSE kbd-shiftllock FALSE kbd-shiftrlock FALSE kbd-ctrlllock FALSE kbd-ctrlrlock FALSE usb-gadget FALSE usb-host FALSE mmc1 FALSE timer FALSE oneshot FALSE disk-activity FALSE ide-disk FALSE heartbeat FALSE cpu0 FALSE cpu1 FALSE default-on FALSE panic FALSE mmc0 FALSE mmc2 FALSE mmc3 FALSE generic-adc-batt-charging-or-full FALSE generic-adc-batt-charging FALSE generic-adc-batt-full FALSE generic-adc-batt-charging-blink-full-solid FALSE bq27421-0-charging-or-full FALSE bq27421-0-charging FALSE bq27421-0-full FALSE bq27421-0-charging-blink-full-solid FALSE bq24297-online FALSE rfkill0);
fi

echo $trigger > /sys/class/leds/$led/trigger
