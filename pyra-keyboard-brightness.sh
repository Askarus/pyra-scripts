#!/bin/sh

minbright=0
maxbright="$(cat /sys/devices/platform/keyboard-backlight/backlight/keyboard-backlight/max_brightness)"
curbright="$(cat /sys/devices/platform/keyboard-backlight/backlight/keyboard-backlight/actual_brightness)"
if [ ! $1 ]; then
newbright=$(zenity --scale --text "pick a number" --min-value=$minbright --max-value=$maxbright --value=$curbright --step 1);
else
newbright=$1
fi
if [ $newbright ]; then
	if [ $newbright -le $minbright ]; then newbright=$minbright; fi
	if [ $newbright -ge $maxbright ]; then newbright=$maxbright; fi
	echo $newbright > /sys/devices/platform/keyboard-backlight/backlight/keyboard-backlight/brightness
fi
