#!/bin/sh

minbright=1
maxbright="$(cat /sys/devices/platform/backlight/backlight/backlight/max_brightness)"
curbright="$(cat /sys/devices/platform/backlight/backlight/backlight/actual_brightness)"
if [ ! $1 ]; then
newbright=$(zenity --scale --text "pick a number" --min-value=$minbright --max-value=$maxbright --value=$curbright --step 1);
else
newbright=$1
fi
if [ $newbright ]; then
	if [ $newbright -le $minbright ]; then newbright=$minbright; fi
	if [ $newbright -ge $maxbright ]; then newbright=$maxbright; fi
	echo $newbright > /sys/devices/platform/backlight/backlight/backlight/brightness
fi
