#!/bin/sh

minbright=1
curbright="$(cat /sys/devices/platform/backlight/backlight/backlight/actual_brightness)"
if [ "$curbright" = "$minbright" ]; then
newbright=$minbright
else
newbright=$(($curbright-1))
fi

echo $newbright > /sys/devices/platform/backlight/backlight/backlight/brightness

