#!/bin/sh

curbright="$(cat /sys/devices/platform/keyboard-backlight/backlight/keyboard-backlight/actual_brightness)"

if [ $curbright = 0 ]; then
	newbright="$( cat /var/lib/pyra/keyboard-toggle-value)"
else
	echo $curbright > /var/lib/pyra/keyboard-toggle-value
	newbright=0	
fi

echo $newbright > /sys/devices/platform/keyboard-backlight/backlight/keyboard-backlight/brightness
