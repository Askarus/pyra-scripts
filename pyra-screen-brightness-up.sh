#!/bin/sh

maxbright="$(cat /sys/devices/platform/backlight/backlight/backlight/max_brightness)"
curbright="$(cat /sys/devices/platform/backlight/backlight/backlight/actual_brightness)"

if [ $curbright = $maxbright]; then
newbright=$maxbright
else
newbright=$(($curbright+1))
fi

echo $newbright > /sys/devices/platform/backlight/backlight/backlight/brightness

