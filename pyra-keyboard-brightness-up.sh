#!/bin/sh

maxbright="$(cat /sys/devices/platform/keyboard-backlight/backlight/keyboard-backlight/max_brightness)"
curbright="$(cat /sys/devices/platform/keyboard-backlight/backlight/keyboard-backlight/actual_brightness)"

if [ $curbright = $maxbright]; then
newbright=$maxbright
else
newbright=$(($curbright+1))
fi

echo $newbright > /sys/devices/platform/keyboard-backlight/backlight/keyboard-backlight/brightness

