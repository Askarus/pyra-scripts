#!/bin/sh

minbright=0
curbright="$(cat /sys/devices/platform/keyboard-backlight/backlight/keyboard-backlight/actual_brightness)"
if [ "$curbright" = "$minbright" ]; then
newbright=$minbright
else
newbright=$(($curbright-1))
fi

echo $newbright > /sys/devices/platform/keyboard-backlight/backlight/keyboard-backlight/brightness

